#include <iostream>
#include "lenguaje.h"

void clear();

void clear()
{
    // CSI[2J clears screen, CSI[H moves the cursor to top-left corner
    std::cout << "\x1B[2J\x1B[H";
}

int main() {

    /***********************************           Menú            ******************************************/

    unsigned int op = 0;

    clear();

    do {

        std::cout << std::endl << std::endl;
        std::cout << "------ Calculadora de Lenguajes -------" << std::endl;
        std::cout << std::endl;
        std::cout << "1. Inversa" << std::endl;
        std::cout << "2. Concatenación" << std::endl;
        std::cout << "3. Unión" << std::endl;
        std::cout << "4. Intersección" << std::endl;
        std::cout << "5. Diferencia" << std::endl;
        std::cout << "6. Sublenguajes" << std::endl;
        std::cout << "7. Igualdad de lenguajes" << std::endl;
        std::cout << "8. Potencia" << std::endl;
        std::cout << "9. Cierre de Kleene" << std::endl;
        std::cout << "10. Salir" << std::endl;
        std::cout << std::endl;
        std::cout << ">>> Introduzca una opción: ";
        std::cin >> op;


        while ( op<1 || op>10 ) {

            clear();
            std::cout << "------ Calculadora de Lenguajes -------" << std::endl;
            std::cout << std::endl;
            std::cout << "1. Inversa" << std::endl;
            std::cout << "2. Concatenación" << std::endl;
            std::cout << "3. Unión" << std::endl;
            std::cout << "4. Intersección" << std::endl;
            std::cout << "5. Diferencia" << std::endl;
            std::cout << "6. Sublenguajes" << std::endl;
            std::cout << "7. Igualdad de lenguajes" << std::endl;
            std::cout << "8. Potencia" << std::endl;
            std::cout << "9. Cierre de Kleene" << std::endl;
            std::cout << "10. Salir" << std::endl;
            std::cout << std::endl;
            std::cout << ">>> Introduzca una opción: ";
            std::cin >> op;

        }

        std::string cadenas;
        Lenguaje* L1;
        Lenguaje* L2;

        switch (op) {

            case 1:         // INVERSA
            {

                clear();
                std::cout << ">>> Introduzca un Lenguaje: ";
                std::cin >> cadenas;

                L1 = new Lenguaje(cadenas);

                std::cout << "Inversa: " << L1->inversa();

                delete L1;

                op = 20;
                break;
            }
            case 2:         // CONCATENACION
            {
                clear();
                std::cout << ">>> Introduzca un Lenguaje L1: ";
                std::cin >> cadenas;

                L1 = new Lenguaje(cadenas);

                cadenas.clear();

                std::cout << ">>> Introduzca un segundo Lenguaje L2: ";
                std::cin >> cadenas;

                L2 = new Lenguaje(cadenas);



                std::cout << "Concatenación: " << L1->concatenacion(*L2);

                // Si ambos lenguajes estan formados a base de una expresion regular, los mostramos concatenados

                if(L1->isExreg_() && L2->isExreg_())

                    std::cout << "Expresión regular: " << L1->get_Ex_reg_() + L2->get_Ex_reg_() << std::endl;

                delete L1;
                delete L2;


                op = 20;
                break;
            }
            case 3:         // UNION
            {
                clear();
                std::cout << ">>> Introduzca un Lenguaje L1: ";
                std::cin >> cadenas;

                L1 = new Lenguaje(cadenas);

                cadenas.clear();

                std::cout << ">>> Introduzca un segundo Lenguaje L2: ";
                std::cin >> cadenas;

                L2 = new Lenguaje(cadenas);

                std::cout << "Unión: " << L1->union_(*L2);

                // Si ambos lenguajes estan formados a base de una expresion regular, los mostramos unidos

                if(L1->isExreg_() && L2->isExreg_())

                    std::cout << "Expresión regular: " << L1->get_Ex_reg_() << "|" << L2->get_Ex_reg_() << std::endl;

                delete L1;
                delete L2;


                op = 20;
                break;
            }
            case 4:         // INTERSECCION
            {
                clear();
                std::cout << ">>> Introduzca un Lenguaje L1: ";
                std::cin >> cadenas;

                L1 = new Lenguaje(cadenas);

                cadenas.clear();

                std::cout << ">>> Introduzca un segundo Lenguaje L2: ";
                std::cin >> cadenas;

                L2 = new Lenguaje(cadenas);

                std::cout << "Intersección: " << L1->interseccion(*L2);

                delete L1;
                delete L2;


                op = 20;
                break;
            }
            case 5:         // DIFERENCIA
            {
                clear();
                std::cout << ">>> Introduzca un Lenguaje L1: ";
                std::cin >> cadenas;

                L1 = new Lenguaje(cadenas);

                cadenas.clear();

                std::cout << ">>> Introduzca un segundo Lenguaje L2: ";
                std::cin >> cadenas;

                L2 = new Lenguaje(cadenas);

                std::cout << "Diferencia: " << L1->diferencia(*L2);

                delete L1;
                delete L2;


                op = 20;
                break;
            }
            case 6:         // SUBLENGUAJES
            {
                clear();
                std::cout << ">>> Introduzca un Lenguaje L1: ";
                std::cin >> cadenas;

                L1 = new Lenguaje(cadenas);

                cadenas.clear();

                std::cout << ">>> Introduzca un segundo Lenguaje L2: ";
                std::cin >> cadenas;

                L2 = new Lenguaje(cadenas);

                if(L2->sublenguaje(*L1))

                    std::cout << "L2 es sublenguaje de L1" << std::endl;
                else
                    std::cout << "L2 no es sublenguaje de L1" << std::endl;

                delete L1;
                delete L2;

                op = 20;
                break;
            }
            case 7:         // IGUALDAD DE LENGUAJES
            {
                clear();
                std::cout << ">>> Introduzca un Lenguaje L1: ";
                std::cin >> cadenas;

                L1 = new Lenguaje(cadenas);

                cadenas.clear();

                std::cout << ">>> Introduzca un segundo Lenguaje L2: ";
                std::cin >> cadenas;

                L2 = new Lenguaje(cadenas);

                if(L1->igualdad_lenguajes(*L2))

                    std::cout << "L1 es igual que L2" << std::endl;
                else
                    std::cout << "L1 no es igual que L2" << std::endl;

                delete L1;
                delete L2;


                op = 20;
                break;
            }
            case 8:         // POTENCIA
            {
                clear();
                std::cout << ">>> Introduzca un Lenguaje: ";
                std::cin >> cadenas;

                L1 = new Lenguaje(cadenas);

                unsigned int n = 0;

                std::cout << ">>> Introduzca el numero de veces que desea hacer la potencia: ";
                std::cin >> n;

                std::cout << "Potencia: " << L1->potencia(n);

                delete L1;

                op = 20;
                break;
            }
            case 9:         // CIERRE DE KLEENE
            {

                clear();

                std::cout << ">>> Introduzca un Lenguaje: ";
                std::cin >> cadenas;

                L1 = new Lenguaje(cadenas);

                std::cout << "Cierre de Kleene: ";
                *L1 = L1->cierre_kleene();

                // Mostrar por pantalla la solucion

                unsigned long j=1;

                std::cout << "{";

                for(auto &i: L1->getCadenas_()) {

                    std::cout << i;

                    if(j < L1->getCadenas_().size()) {

                        std::cout << ",";
                    }
                }

                std::cout << " ... }" << std::endl;

                if(L1->isExreg_())

                    std::cout << "Expresión regular: " << L1->get_Ex_reg_() << "*" << std::endl;

                delete L1;
                op = 20;
                break;
            }
            case 10:        // SALIR
            {
                break;
            }
            default: {
                break;
            }
        }

    } while ( op<1 || op>10 );
}
