#include <set>
#include <string>
#include <iostream>

#include "Exreg.h"


class Lenguaje {

private:

    std::set<std::string> cadenas_;
    bool vacio_;
    Exreg* ex_reg_;


public:

    Lenguaje();
    Lenguaje(const Lenguaje& l);
    Lenguaje(const std::set<std::string>& cadenas);

    Lenguaje(std::string& cadenas);
    ~Lenguaje();

    std::set<std::string> get_lenguaje(std::string& cadenas);

    bool isVacio_() const;
    void setVacio_(bool vacio_);
    const std::set<std::string> &getCadenas_() const;
    bool isExreg_() const;
    std::string get_Ex_reg_() const;

    Lenguaje potencia(unsigned int n);
    Lenguaje concatenacion(Lenguaje& l);
    Lenguaje union_(Lenguaje& l);
    Lenguaje interseccion(Lenguaje& l);
    Lenguaje diferencia(Lenguaje& l);
    bool sublenguaje(Lenguaje& l);
    bool igualdad_lenguajes(Lenguaje& l);
    Lenguaje inversa();
    Lenguaje cierre_kleene();

    friend std::ostream& operator<<(std::ostream& os, const Lenguaje& lenguaje);
    Lenguaje operator+=(const Lenguaje& lenguaje);

};
