#pragma once

#include <string>
#include <set>
#include <regex>

class Exreg {       // Clase para interpretar las expresiones regulares y obtener cadenas de dicho lenguaje

private:

    std::string expresion_;     // expresion regular como string
    std::regex reg_exp_;        // libreria para interpretar la expresion regular

public:

    Exreg();
    Exreg(std::string expresion);
    ~Exreg();

    const std::string& getExpresion_() const;

    std::set<std::string> obtener_cadenas();

};


