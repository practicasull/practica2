//
// Created by naomi on 2/10/16.
//

#include <iostream>
#include "Exreg.h"
#include "lenguaje.h"

Exreg::Exreg() {}

Exreg::Exreg(std::string expresion):
expresion_(expresion)

{
    //obtener_cadenas();
}

Exreg::~Exreg() {

}

std::set<std::string> Exreg::obtener_cadenas() {

    // Obtener alfabeto

    std::set<std::string> alfabeto;

    for (auto &i: expresion_) {

        if (i != '(' && i != ')' && i != '*' && i != '+' && i != '|') {

            std::string str;
            str = i;

            alfabeto.insert(str);
        }
    }

    Lenguaje L(alfabeto);

    L = L.cierre_kleene();

    std::set<std::string> cadenas_aceptadas;

    reg_exp_ = expresion_;

    for (auto &j: L.getCadenas_()) {

        std::smatch str_match_result;
        std::regex_match(j,str_match_result,reg_exp_);

        for (auto &i: str_match_result)

            cadenas_aceptadas.insert(i);

    }

    return cadenas_aceptadas;

}

const std::string& Exreg::getExpresion_() const
{
    return expresion_;
}
