
#include "lenguaje.h"

Lenguaje::Lenguaje():
cadenas_(),
vacio_(true)
{}

Lenguaje::Lenguaje(const Lenguaje &l) {

    this->cadenas_ = l.cadenas_;
    this->vacio_ = l.vacio_;
    this->ex_reg_ = l.ex_reg_;
}

Lenguaje::Lenguaje(const std::set<std::string>& cadenas):
cadenas_(cadenas)
{
    vacio_ = cadenas_.size() == 0;
}

Lenguaje::Lenguaje(std::string& cadenas):
cadenas_(),
vacio_()
{
    cadenas_ = get_lenguaje(cadenas);
}

Lenguaje::~Lenguaje() {

}

std::set<std::string> Lenguaje::get_lenguaje(std::string& cadenas) {

    unsigned long pos;
    bool last_seq = false;

    std::set<std::string> lenguaje;

    if(cadenas[0] == '{' && cadenas[1] == '}'){

        // Lenguaje vacio

        this->setVacio_(true);
    }
    else{

        // Lenguaje no vacio a base de cadenas o expr regular

        if (cadenas[0] != '{') {

            this->setVacio_(false);

            ex_reg_ = new Exreg(cadenas);
            lenguaje = ex_reg_->obtener_cadenas();

            std::cout << "Cadenas de la expresion regular: ";

            for (auto &i:lenguaje)
                std::cout <<  i << " ";

            std::cout << std::endl;

        }
        else {

            // Lenguaje no vacio a base de cadenas (sin espacios. Con espacios no funciona {a, b} <- NO {a,b} <- Si

            while (cadenas.size() > 2) {

                pos = cadenas.find(",");

                if (pos == std::string::npos) {

                    pos = cadenas.find("}");
                    last_seq = true;
                }

                std::string aux = cadenas.substr(1, pos - 1); // comprobar si es pos-1 o pos

                if (!last_seq)

                    cadenas.erase(1, pos);       // Si todavia quedan cadenas por sustraer, borra hasta la primera ','

                else

                    cadenas.erase(1, pos - 1);     // Si es la ultima cadena, evita que elimine '}'

                lenguaje.insert(aux);

            }
        }
    }

    return lenguaje;

}

Lenguaje Lenguaje::potencia(unsigned int n) {      // Si n = 0; lengu. vacio, si n=1, lenguaje

    if(this->isVacio_())

        return *this;

    if(n == 0){

        return std::set<std::string>();
    }
    else if(n == 1){

        Lenguaje solucion;

        solucion.cadenas_ = this->cadenas_;

        return solucion;
    }
    else {

        Lenguaje solucion;
        Lenguaje solucion_aux(*this);

        for (unsigned int i = 1; i < n; i++) {

            solucion = concatenacion(solucion_aux);
            solucion_aux.cadenas_ = solucion.cadenas_;
        }

        return solucion;
    }
}

Lenguaje Lenguaje::concatenacion(Lenguaje& l) {         // ToDo: Concatenar dos expresiones regulares

    std::set<std::string> solucion;


    for (auto& i: this->cadenas_) {

        for (auto& j: l.cadenas_) {

            solucion.insert(i + j);
        }
    }

    return Lenguaje(solucion);
}

Lenguaje Lenguaje::union_(Lenguaje& l) {

    std::set<std::string> solucion;

    for(auto &i: this->cadenas_)

        solucion.insert(i);

    for(auto &i: l.cadenas_)

        solucion.insert(i);

    return Lenguaje(solucion);
}

Lenguaje Lenguaje::interseccion(Lenguaje& l) {

    std::set<std::string> solucion;

    for(auto &i: this->cadenas_)

        for (auto &j: l.cadenas_)

            if (i == j)

                solucion.insert(i);

    return Lenguaje(solucion);
}

Lenguaje Lenguaje::diferencia(Lenguaje& l) {

    std::set<std::string> solucion;

    for(auto &i: this->cadenas_) {

        bool aux = true;

        for (auto &j: l.cadenas_) {

            if (i == j)

                aux = false;
        }

        if(aux)

            solucion.insert(i);

    }

    return Lenguaje(solucion);
}

bool Lenguaje::sublenguaje(Lenguaje &l) {

    if(this->isVacio_())

        return false;

    bool sub = true;

    for(auto &i: this->cadenas_){

        bool aux = false;

        for(auto &j: l.cadenas_){

            if(i == j)

                aux = true;

        }

        if(!aux)

            sub = false;
    }

    return sub;
}

bool Lenguaje::igualdad_lenguajes(Lenguaje &l) {

    bool idem = true;

    if(this->cadenas_.size() != l.cadenas_.size())

        return false;

    else {

        for (auto &i: this->cadenas_) {

            bool aux = false;

            for (auto &j: l.cadenas_) {

                if (i == j)

                    aux = true;

            }

            if (!aux)

                idem = false;
        }
    }

    return idem;
}

Lenguaje Lenguaje::inversa() {

    std::set<std::string> solucion;

    for(auto &i: this->cadenas_){

        std::string aux;

        for(unsigned int j= (unsigned int) (i.size() - 1); j >= 0 && j < i.size(); j--){

            std::string str;
            str = i[j];

            aux = aux + str;

        }

        solucion.insert(aux);
    }
    Lenguaje sol(solucion);

    return sol;
}

Lenguaje Lenguaje::cierre_kleene() {

    for(unsigned int i=1;i<4;++i)           // Cambiado a 4

        *this += this->potencia(i);

    this->cadenas_.insert("&");

    return *this;
}

std::ostream& operator<<(std::ostream& os, const Lenguaje& lenguaje)
{

    unsigned long j = 1;

    os << "{";

    for (auto &i: lenguaje.cadenas_) {

        os << i;

        if ( j<lenguaje.cadenas_.size() ) {

            os << ",";
            j++;
        }
    }

    os << "}" << std::endl;

    return os;
}

bool Lenguaje::isVacio_() const {
    return vacio_;
}

void Lenguaje::setVacio_(bool vacio_) {
    Lenguaje::vacio_ = vacio_;
}

bool Lenguaje::isExreg_() const
{

    return ex_reg_ != nullptr;
}

std::string Lenguaje::get_Ex_reg_() const
{
    std::string str;
    str = "(" + this->ex_reg_->getExpresion_() + ")";
    return str;
}

const std::set<std::string> &Lenguaje::getCadenas_() const {
    return cadenas_;
}

Lenguaje Lenguaje::operator+=(const Lenguaje &lenguaje) {

    for(auto &i: lenguaje.getCadenas_())

        this->cadenas_.insert(i);

    return *this;
}


